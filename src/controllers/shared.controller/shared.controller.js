import fs from 'fs';
import ApiError from '../../helpers/ApiError';
import { validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';
import { toImgUrl, toFileUrl } from '../../utils';
import isImage from 'is-image';


function deleteTempImages(req) {
  if (req.file || req.files) {
    console.log(req.files);
    let files = req.file ? Array.from(req.file) : req.files;
    console.log(files);
    for (let file of files) {
      fs.unlink(file.path, function (err) {
        if (err) return console.log(err);
        // Under Experimental 
        console.log(file.filename + ' deleted successfully');
      });
    }
  }
}

export const localeFn = (localeName) => (value, { req }) => req.__(localeName);

export function checkValidations(req) {

  const validationErrors = validationResult(req).array({ onlyFirstError: true });

  if (validationErrors.length > 0) {
    // deleteTempImages(req);
    throw new ApiError(422, validationErrors);
  }

  return matchedData(req);
}


export async function handleImgs(req, { attributeName = 'images', isUpdate = false } = {}, checkType = false) {
  if (req.files && req.files.length > 0 || (isUpdate && req.body[attributeName])) { // .files contain an array of 'images'  
    let images = [];
    if (isUpdate && req.body[attributeName]) {
      if (Array.isArray(req.body[attributeName]))
        images = req.body[attributeName];
      else
        images.push(req.body[attributeName]);
    }

    for (const img of req.files) {
      if (!checkType) {
        images.push(await toImgUrl(req, img));
      }
      else {
        let url = await toImgUrl(req, img);
        let type = 'VIDEO'
        if (await isImage(url)) {
          type = 'IMAGE'
        }
        images.push({
          url: url,
          type: type
        })
      }
    }
    return images;
  }
  throw new ApiError.UnprocessableEntity(`${attributeName} are required`);
}

export async function handleImg(req, { attributeName = 'img', isUpdate = false } = {}, file) {
  if (file) {
    return await toImgUrl(null, file);
  }
  if (req.file || (isUpdate && req.body[attributeName])) // .file contain an 'image'
    return req.body[attributeName] || await toImgUrl(req, req.file);


  throw new ApiError.UnprocessableEntity(`${attributeName} is required`);
}

export async function handleFiles(req, { attributeName = 'files', isUpdate = false } = {}) {
  if (req.files && req.files.length > 0 || (isUpdate && req.body[attributeName])) {
    let files = [];
    if (isUpdate && req.body[attributeName]) {
      if (Array.isArray(req.body[attributeName]))
        files = req.body[attributeName];
      else
        files.push(req.body[attributeName]);
    }

    for (const file of req.files) {
      files.push(await toFileUrl(req, file));
    }
    return files;
  }
  throw new ApiError.UnprocessableEntity(`${attributeName} are required`);
}
export function parseObject(arrayOfFields, update = false, fieldName = 'body') {
  return (req, res, next) => {
    try {
      //console.log(arrayOfFields)
      //console.log('arrayOfFields')
      for (let index = 0; index < arrayOfFields.length; index++) {
        //console.log('index ',index)
        var name = arrayOfFields[index];
        if (req[fieldName][name]) {
          console.log(req[fieldName][name])
          req[fieldName][name] = JSON.parse(req[fieldName][name]);
        }

      }
      return next()
    } catch (error) {
      return next(error);
    }
  }
}
export async function fieldhandleImg(req, { attributeName = 'images', isUpdate = false } = {}) {
  if (req.files && req.files[attributeName].length > 0 || (isUpdate && req.body[attributeName])) { // .files contain an array of 'images'  
    let images;
    images = await toImgUrl(req, req.files[attributeName][0]);
    return images;
  }
  throw new ApiError.UnprocessableEntity(`${attributeName} are required`);
}
