import bcrypt from 'bcryptjs';
import { body } from 'express-validator/check';
import { checkValidations, handleImg, fieldhandleImg } from '../shared.controller/shared.controller';
import { generateToken } from '../../utils/token';
import User from '../../models/user.model/user.model';
import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import ApiError from '../../helpers/ApiError';
import i18n from 'i18n'
import ApiResponse from '../../helpers/ApiResponse';
const populateQuery = [
];

export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            var { username, email ,firstName,lastName} = req.query;
            var query = { deleted: false };
            if (username) query.username = { '$regex': username, '$options': 'i' };
            if (firstName) query.firstName = { '$regex': firstName, '$options': 'i' };
            if (lastName) query.lastName = { '$regex': lastName, '$options': 'i' };
            if (email) query.email = { '$regex': email, '$options': 'i' };
            let users;
            users = await User.find(query).sort({ _id: -1 }).limit(limit).skip((page - 1) * limit);
            const userCount = await User.count(query);
            const pageCount = Math.ceil(userCount / limit);
            res.send(new ApiResponse(users, page, pageCount, limit, userCount, req));
        } catch (error) {
            next(error)
        }
    },

    validateUserSignin() {
        let validations = [
            body('email').trim().not().isEmpty().withMessage(() => { return i18n.__('emailRequiredPhone') }),
            body('password').not().isEmpty().withMessage(() => { return i18n.__('passwordRequired') })];
        return validations;
    },
    async signIn(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            validatedBody.email = (validatedBody.email.trim()).toLowerCase();
            var query = { deleted: false, email: validatedBody.email};
            let user = await User.findOne(query);

            if (user) {
                user = await User.populate(user, populateQuery);

                bcrypt.compare(validatedBody.password, user.password, async function (err, isMatch) {
                    if (!isMatch)
                        return next(new ApiError(403, i18n.__('invalidPassword')));
                    res.status(200).send({
                        user: user,
                        token: generateToken(user.id)
                    });
                });
            }
            else {
                return next(new ApiError(403, i18n.__('userNotFound')));
            }
        } catch (err) {
            next(err);
        }
    },
    validateUserCreateBody() {
        let validations = [
            body('username').not().isEmpty().withMessage(() => { return i18n.__('usernameRequired') })
            .custom(async (value, { req }) => {
                let userQuery = { username: value, deleted: false};
                if (await User.findOne(userQuery))
                    throw new Error(i18n.__('usernameDuplicated'));
                else
                    return true;
            }),
            body('firstName').not().isEmpty().withMessage(() => { return i18n.__('firstNameRequired') }),
            body('lastName').not().isEmpty().withMessage(() => { return i18n.__('lastNameRequired') }),
            body('email').trim().not().isEmpty().withMessage(() => { return i18n.__('emailRequired') })
                .isEmail().withMessage(() => { return i18n.__('EmailNotValid') })
                .custom(async (value, { req }) => {
                    value = (value.trim()).toLowerCase();
                    let userQuery = { email: value, deleted: false};
                    if (await User.findOne(userQuery))
                        throw new Error(i18n.__('emailDuplicated'));
                    else
                        return true;
                }),
            body('password').not().isEmpty().withMessage(() => { return i18n.__('passwordRequired') })
        ];
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            validatedBody.email = (validatedBody.email.trim()).toLowerCase();
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'image' });
                validatedBody.image = image;
            }
            console.log(validatedBody);

            let createdUser = await User.create({
                ...validatedBody,
            });
            res.status(201).send({
                user: createdUser,
                token: generateToken(createdUser.id)
            });

        } catch (err) {
            next(err);
        }
    },

    validateUserUpdate() {
        let validations = [
            body('username').optional().not().isEmpty().withMessage(() => { return i18n.__('usernameRequired') })
            .custom(async (value,{req}) => {
                let userQuery = { username: value, deleted: false,_id : { $ne: req.user._id }};
                if (await User.findOne(userQuery))
                    throw new Error(i18n.__('usernameDuplicated'));
                else
                    return true;
            }),
            body('firstName').optional().not().isEmpty().withMessage(() => { return i18n.__('firstNameRequired') }),
            body('lastName').optional().not().isEmpty().withMessage(() => { return i18n.__('lastNameRequired') }),
            body('email').optional().trim().not().isEmpty().withMessage(() => { return i18n.__('emailRequired') })
                .isEmail().withMessage(() => { return i18n.__('EmailNotValid') })
                .custom(async (value,{req}) => {
                    value = (value.trim()).toLowerCase();
                    let userQuery = { email: value, deleted: false,_id : { $ne: req.user._id }};
                    if (await User.findOne(userQuery))
                        throw new Error(i18n.__('emailDuplicated'));
                    else
                        return true;
                }),
            body('newPassword').optional().not().isEmpty().withMessage(() => { return i18n.__('newPasswordRequired') }),
            body('currentPassword').optional().not().isEmpty().withMessage(() => { return i18n.__('CurrentPasswordRequired') }),
            body('password').optional().not().isEmpty().withMessage(() => { return i18n.__('passwordRequired') }),

        ];

        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkExistThenGet(req.user.id, User, { deleted: false });
            console.log(validatedBody)
            if (validatedBody.newPassword) {
                if (validatedBody.currentPassword) {
                    if (bcrypt.compareSync(validatedBody.currentPassword, user.password)) {
                        const salt = bcrypt.genSaltSync();
                        var hash = await bcrypt.hash(validatedBody.newPassword, salt);
                        validatedBody.password = hash;
                        delete validatedBody.newPassword;
                        delete validatedBody.currentPassword;
                    }
                    else {
                        return next(new ApiError(403, i18n.__('currentPasswordInvalid')))
                    }
                }
                else {
                    return res.status(400).send({
                        error: [
                            {
                                location: 'body',
                                param: 'currentPassword',
                                msg: i18n.__('CurrentPasswordRequired')
                            }
                        ]
                    });
                }
            }
            if (validatedBody.password) {
                const salt = bcrypt.genSaltSync();
                var hash = await bcrypt.hash(validatedBody.password, salt);
                validatedBody.password = hash;
            }
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'image', isUpdate: false });
                validatedBody.image = image;
            }
            user = await User.findOneAndUpdate({ deleted: false, _id: req.user.id }, validatedBody, { new: true }).populate(populateQuery);
            res.status(200).send(user);

        } catch (error) {
            next(error);
        }
    },

    async userInformation(req, res, next) {
        try {
            var userId = req.query.userId;
            var user = await User.findOne({ _id: userId, deleted: false }).populate(populateQuery);
            if (!user)
                throw new ApiError(403, i18n.__('userNotFound'));
            res.status(200).send({ user: user });
        } catch (error) {
            next(error);
        }
    },

    async deleteAccount(req, res, next) {
        try {
            let {userId} = req.params;
            var user = await checkExistThenGet(userId, User, { deleted: false });
            user.deleted = true;
            await user.save();
            res.status(200).send('Deleted Successfully');
        } catch (error) {
            next(error);
        }
    }
};