import Post from "../../models/post.model/post.model";
import User from "../../models/user.model/user.model";
import ApiResponse from "../../helpers/ApiResponse";
import ApiError from "../../helpers/ApiError";
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods";
import { body } from 'express-validator/check';
import { checkValidations } from "../shared.controller/shared.controller";
import { sendEmail } from '../../services/emailMessage.service';
import i18n from 'i18n';
let populateQuery = [
    { path: 'mentionUsers', model: 'user' },
    { path: 'user', model: 'user' }
];

export default {

    validateBody(isUpdate = false) {
        let validations
        if (!isUpdate) {
            validations = [
                body('text').not().isEmpty().withMessage(() => { return i18n.__('textRequired') })
                .custom(async (val) => {
                    if(val.length > 140){
                        throw new Error(i18n.__('textLimit'));
                    }
                }),
                body('mentionUsers').optional().not().isEmpty().withMessage(() => { return i18n.__('mentionUsersRequired') }).isArray()
                    .custom(async (val) => {
                        for (let index = 0; index < val.length; index++) {
                            await checkExist(val[index], User, { deleted: false });
                            return true;
                        }
                    })
            ];
        }
        else {
            validations = [
                body('text').optional().not().isEmpty().withMessage(() => { return i18n.__('textRequired') }),
                body('mentionUsers').optional().not().isEmpty().withMessage(() => { return i18n.__('mentionUsersRequired') }).isArray()
                    .custom(async (val) => {
                        for (let index = 0; index < val.length; index++) {
                            await checkExist(val[index], User, { deleted: false });
                            return true;
                        }
                    })
            ];
        }
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            var { user, text } = req.query;
            let query = { deleted: false };
            if (text) query.text = { '$regex': text, '$options': 'i' };
            if (user) query.user = user;

            let posts = await Post.find(query).populate(populateQuery).sort({ createdAt: -1 }).limit(limit).skip((page - 1) * limit);

            const postsCount = await Post.count(query);
            const pageCount = Math.ceil(postsCount / limit);
            res.send(new ApiResponse(posts, page, pageCount, limit, postsCount, req));

        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            let user = req.user;
            let validatedBody = checkValidations(req);
            validatedBody.user = user.id;
            let post = await Post.create(validatedBody);
            post = await Post.populate(post,populateQuery)
            if(post.mentionUsers.length > 0){
                for (let index = 0; index < post.mentionUsers.length; index++) {
                    await sendEmail(post.mentionUsers[index].email,user.username + ' mentioned you in a post.' ); 
                }
            }
            res.status(200).send(post);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { postId } = req.params;
            let user = req.user;
            let post = await checkExistThenGet(postId, Post, { deleted: false });
            if (post.user != user.id) {
                return next(new ApiError(403, ('Not allowed to update this post.')));
            }
            let validatedBody = checkValidations(req);
            let updatedPost = await Post.findByIdAndUpdate(postId, {
                ...validatedBody,
            }, { new: true }).populate(populateQuery);
            res.status(200).send(updatedPost);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let user = req.user;
            let { postId } = req.params;
            let post = await checkExistThenGet(postId, Post, { deleted: false, populate: populateQuery });
            res.status(200).send(post);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            let { postId } = req.params;
            let post = await checkExistThenGet(postId, Post, { deleted: false });
            if (post.user != user.id) {
                return next(new ApiError(403, ('Not allowed to delete this post.')));
            }
            post.deleted = true;
            await post.save();
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}