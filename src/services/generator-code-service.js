import generator from 'password-generator';

export const generateVerifyCode = () => generator(6,false);
