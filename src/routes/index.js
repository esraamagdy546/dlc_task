import express from 'express';
const router = express.Router();
import userRoute from './user.route/user.route';
import postRoute from './post.route/post.route';


router.use('/user',userRoute);
router.use('/post',postRoute);

export default router;
 