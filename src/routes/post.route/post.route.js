var express = require('express');
var router = express.Router();
import postController from "../../controllers/post.controller/post.controller";
import {requireAuth} from '../../services/passport';

router.route('/')
    .get(requireAuth,postController.findAll)
    .post(requireAuth,postController.validateBody(),postController.create)


router.route('/:postId')
    .get(requireAuth,postController.findById)
    .put(requireAuth,postController.validateBody(true),postController.update)
    .delete(requireAuth,postController.delete)

export default router;