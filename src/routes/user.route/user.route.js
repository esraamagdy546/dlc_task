import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';
import userController from '../../controllers/user.controller/user.controller';

const router = express.Router();

router.route('/signup')
    .post(
        multerSaveTo('users').single('image'),
        userController.validateUserCreateBody(),
        userController.signUp
    );

router.post('/login', userController.validateUserSignin(), userController.signIn);

router.get('/allUsers', userController.findAll);
router.get('/userInfo', requireAuth, userController.userInformation)


router.put('/updateInfo',
    requireAuth,
    multerSaveTo('users').single('image'),
    userController.validateUserUpdate(true),
    userController.updateInfo);

router.route('/:userId/account').delete(requireAuth,userController.deleteAccount);

export default router;
