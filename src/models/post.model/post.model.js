import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const CarSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user:{
        type: Number,
        required: true,
        ref:'user'
    },
    text:{
        type: String,
        required: true
    },
    mentionUsers:{
        type: [Number],
        ref:'user'
    },
    deleted:{
        type:Boolean,
        default:false
    }
       
}, { timestamps: true });

CarSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);

CarSchema.plugin(autoIncrement.plugin, { model: 'post', startAt: 1 });

export default mongoose.model('post', CarSchema);